idx1=425:525;
idx2=375:475;

M=length(idx1);
N=length(idx2);

load('dfc_2000_processed.mat');
Data1=X(:,:,1:7);
Data2=X(:,:,8);

X1=double(Data1(idx1,idx2,:));
X1=reshape(X1,size(X1,1)*size(X1,2),size(X1,3));
X1=X1./repmat(sqrt(sum(X1.*X1,1)),size(X1,1),1); 
sd1=.1*mean(mean(abs(X1)));
X1=X1+sd1*randn(size(X1));

X2=double(Data2(idx1,idx2,:));
X2=reshape(X2,size(X2,1)*size(X2,2),1);
X2=X2*sum(sum(X1.^2))/sum(X2.^2)*size(X1,2);
sd2=.1*mean(mean(abs(X2)));
X2=X2+sd2*randn(size(X2));

NumClust=10;
DataName='DFC2000';
SpatialWindowSize=2;
NumEigVecs1=2;
NumEigVecs2=2;
NumEigVecsAD=4;
NumEigVecsJoint=4;
NumEigVecsML=4;

