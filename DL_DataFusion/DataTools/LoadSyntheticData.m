idx1=1:60;
idx2=1:60;

sigma_noise=.2;

X1=zeros(60,60);

X1(1:30,:)=1;
X1(31:60,:)=2;

X2=X1';


X1=X1(:);
X2=X2(:);

X1=X1+sigma_noise*randn(size(X1));
X2=X2+sigma_noise*randn(size(X2));

Data1=reshape(X1,length(idx1),length(idx2));
Data2=reshape(X2,length(idx1),length(idx2));

DataName='Synthetic';
NumClust=4;
SpatialWindowSize=3;
NumEigVecs1=2;
NumEigVecs2=2;
NumEigVecsAD=4;
NumEigVecsJoint=4;
NumEigVecsML=6;

