load('casi_2013');
load('lidar_2013');
Data1=x2013_IEEE_GRSS_DF_Contest_CASI;
Data2=x2013_IEEE_GRSS_DF_Contest_LiDAR;

idx1=50:150;
idx2=200:300;

X1=double(x2013_IEEE_GRSS_DF_Contest_CASI(idx1,idx2,:));
X1=reshape(X1,size(X1,1)*size(X1,2),size(X1,3));
X1=X1./repmat(sqrt(sum(X1.*X1,1)),size(X1,1),1); %Normalize X
sd1=.1*mean(mean(abs(X1)));
X1=X1+sd1*randn(size(X1));

X2=double(x2013_IEEE_GRSS_DF_Contest_LiDAR(idx1,idx2));
X2=reshape(X2,size(X2,1)*size(X2,2),1);
X2=X2*sum(sum(X1.^2))/sum(X2.^2)*size(X1,2);
sd2=.1*mean(mean(abs(X2)));
X2=X2+sd2*randn(size(X2));

DataName='DFC2013';
NumClust=10;
SpatialWindowSize=1;
NumEigVecs1=2;
NumEigVecs2=2;
NumEigVecsAD=4;
NumEigVecsJoint=4;
NumEigVecsML=6;

