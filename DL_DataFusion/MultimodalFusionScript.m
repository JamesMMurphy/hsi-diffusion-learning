clear all;
close all;


%% Load and format data

Load2000DFC
%Load2013DFC
%LoadSyntheticData


%% Set parameters

% Number of NN in initial graph construction
kNN=100;

%K-means parameters
NumReplicates=100;
MaxIterations=1000;

% Weight of data source 2 in joint construction; choose it adaptively
lambda=sqrt(sum(X1(:).^2)/sum(X2(:).^2));

%%  Make knn graphs and choose data-dependent scale parameters

Opts.Method='Simple';
A1=Adjacency(X1,kNN,Opts);
A2=Adjacency(X2,kNN,Opts);
A_Joint=Adjacency([X1,lambda.*X2],kNN,Opts);

sigma1=.5*mean(A1(A1>0));
sigma2=.5*mean(A2(A2>0));
sigma_Joint=.5*mean(A_Joint(A_Joint>0));

%% Compute diffusion and Laplacian matrices for the two data sources and the fused data

% Data source 1
[I1,J1]=find(A1>0);
W1=sparse(I1,J1,exp(-A1(A1>0).^2),size(X1,1),size(X1,1));
D1=sparse(diag(sum(W1,2)));
D1_half=sparse(diag(sum(W1,2).^(-.5)));
P1=D1^(-1)*W1; 
L1=sparse(1:size(X1,1),1:size(X1,1),ones(1,size(X1,1)),size(X1,1),size(X1,1))-D1_half*W1*D1_half;

%Data source 2
[I2,J2]=find(A2>0);
W2=sparse(I2,J2,exp(-A2(A2>0).^2),size(X2,1),size(X2,1));
D2=sparse(diag(sum(W2,2)));
D2_half=sparse(diag(sum(W2,2).^(-.5)));
P2=D2^(-1)*W2; 
L2=sparse(1:size(X2,1),1:size(X2,1),ones(1,size(X2,1)),size(X2,1),size(X2,1))-D2_half*W2*D2_half;

%Fused data
[I_Joint,J_Joing]=find(A_Joint>0);
W_Joint=sparse(I_Joint,J_Joing,exp(-A_Joint(A_Joint>0).^2),size(X2,1),size(X2,1));
D_Joint=sparse(diag(sum(W_Joint,2)));
P_Joint=D_Joint^(-1)*W_Joint;


%% Construct alternating diffusion operator

P_AD=P1*P2;

%% Get diffusion kernel spectra

[EigVecs1,EigVals1]=eigs(P1,NumEigVecs1);
[EigVecs2,EigVals2]=eigs(P2,NumEigVecs2);
[EigVecsAD,EigValsAD]=eigs(P_AD,NumEigVecsAD);
[EigVecsJoint,~]=eigs(P_Joint,NumEigVecsJoint);


%% Compute joint diffusion distance on right eigenvectors

DjointReal=pdist(real(EigVecsAD));
DjointImaginary=pdist(imag(EigVecsAD));
Djoint=sqrt(DjointReal.^2+DjointImaginary.^2);
Djoint=squareform(Djoint);

LowDimJoint= cmdscale(Djoint,NumEigVecsJoint);

%% Compute matrix power Laplacians

p=[-2,-1];

for l=1:length(p)
    MPL(:,:,l)=(1/2*(full(L1+log(1+abs(p(l)))*eye(size(L1)))^p(l)+full(L2+log(1+abs(p(l)))*eye(size(L1)))^p(l)))^(1/p(l));
    [EigVecsMPL(:,:,l),EigValsMPL(:,:,l)]=eigs(MPL(:,:,l),NumEigVecsML,'sm');
end

%% Run DL, DLSS on the joint data

%Should we plot everything or not?  Should we save things?
UserPlot=0;
PeakOpts.UserPlot=UserPlot;

%Detect number of eigenvectors to use automatically
PeakOpts.DiffusionOpts.K='automatic'; 

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100;

%Force probability of self-loop to exceed .5.
PeakOpts.DiffusionOpts.LazyWalk=0;

%Set epsilon for weight matrix; use same one for spectral clustering
PeakOpts.DiffusionOpts.epsilon=1;

% Set time to evaluate diffusion distance
PeakOpts.DiffusionTime=30;

%Kmeans parameters
NumReplicates=10;
MaxIterations=100;

%How many nearest neighbors to use for KDE
DensityNN=20;

%Autotune 
PeakOpts.DiffusionOpts.AutoTune=0;

PeakOpts.ModeDetection='Diffusion';
[CentersDiffusion, G_dif, DistStructDiffusion] = DensityPeaksEstimation([X1,lambda.*X2], NumClust, DensityNN, PeakOpts, M,N);

%% Compute Labels

Labels1=kmeans(EigVecs1,NumClust,'Replicates', NumReplicates, 'MaxIter', MaxIterations);
Labels2=kmeans(EigVecs2,NumClust,'Replicates', NumReplicates, 'MaxIter', MaxIterations);
LabelsAD=kmeans(real(EigVecsAD),NumClust,'Replicates', NumReplicates, 'MaxIter', MaxIterations);
LabelsMin=kmeans(real(EigVecsJoint),NumClust,'Replicates', NumReplicates, 'MaxIter', MaxIterations);

for l=1:length(p)
    LabelsML(:,l)=kmeans(EigVecsMPL(:,l),NumClust,'Replicates', NumReplicates, 'MaxIter', MaxIterations);
end

LabelsDL=FSFclustering([X1,lambda.*X2],NumClust,DistStructDiffusion,CentersDiffusion);

[I,J]=find(abs(reshape(X1(:,1),length(idx1),length(idx2)))>0);
LabelsDLSS=SpatialSpectralDiffusionLearning([X1,lambda.*X2],NumClust,DistStructDiffusion,CentersDiffusion,I,J,length(idx1),length(idx2),SpatialWindowSize);


%%  Plots learned labels

% Data source 1 (HSI)
imagesc(sum(Data1(idx1,idx2),3)); 
colorbar
title('Data Source 1','Interpreter','latex');
axis square


% Data source 2 (Lidar)
figure; 
imagesc(Data2(idx1,idx2)); 
colorbar
title('Data Source 2','Interpreter','latex');
colorbar
axis square


% Data source 1 Labels
figure; 
imagesc(reshape(Labels1,length(idx1),length(idx2))); 
colorbar;
title('Data Source 1 only labels','Interpreter','latex');
colorbar
axis square


% Data source 2 labels 
figure; 
imagesc(reshape(Labels2,length(idx1),length(idx2))); 
colorbar
title('Data Soure 2 Only Labels','Interpreter','latex');
colorbar
axis square

% Joint kernel labels
figure; 
imagesc(reshape(LabelsMin,length(idx1),length(idx2))); 
title('Joint Kernel Labels','Interpreter','latex');
colorbar
axis square

% Alternating diffusion labels
figure; 
imagesc(reshape(LabelsAD,length(idx1),length(idx2))); 
title('Alternating Diffusion Labels','Interpreter','latex');
colorbar
axis square

% Matrix power Laplacian labels
for l=1:length(p)
    figure;
    imagesc(reshape(LabelsML(:,l),length(idx1),length(idx2)));
    title(['Matrix Power Laplacian Labels, $p=$',num2str(p(l))],'Interpreter','latex');
    colorbar
    axis square
end

% DL on fused data labels
figure; 
imagesc(reshape(LabelsDL,length(idx1),length(idx2))); 
title('DL Labels','Interpreter','latex');
colorbar
axis square

% DLSS on fused data labels
figure; 
imagesc(reshape(LabelsDLSS,length(idx1),length(idx2))); 
title('DLSS Labels','Interpreter','latex');
axis square
colorbar