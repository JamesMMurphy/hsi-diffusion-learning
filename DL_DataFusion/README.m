% This code performs multimodal data fusion and segmentation using the 
% diffusion learning framework. The following packages are necessary to 
% run this toolbox:
%
% -Diffusion Learning: https://jmurphy.math.tufts.edu/Code/
% -DiffusionGeometry:  https://mauromaggioni.duckdns.org/code/
% 
% Also, the real HSI and lidar data used in the main experimental script
% are from the 2000 and 2013 IEEE Data Fusion Contest data.  This data is
% publically available via IEEE.
%
% The script MultimodalFusionScript.m generates the experiments in the
% paper:
%
% Murphy, James M., and Mauro Maggioni. "Diffusion geometric methods 
% for fusion of remotely sensed data." Algorithms and Technologies for 
% Multispectral, Hyperspectral, and Ultraspectral Imagery XXIV. Vol. 10644. 
% International Society for Optics and Photonics, 2018.
%
% The primary clustering method used to produce the DL and DLSS
% segmentations are described and analyzed in 
%
% -Maggioni, M., J.M. Murphy. Learning by Unsupervised Nonlinear Diffusion. 
% Journal of Machine Learning Research, 20(160), pp. 1-56. 2019.
%
% -Murphy, J.M., Maggioni, M. Unsupervised Clustering and Active Learning 
% of Hyperspectral Images with Nonlinear Diffusion. IEEE Transactions on 
% Geoscience and Remote Sensing, 57(3), pp. 1829-1845. 2019.
% 
% Users of this code are free to modify as they wish.  If you find the code,
% useful and wish to use it any publucations, please cite the relevant papers
% listed above.
%
% If you have questions, please write to jm.murphy@tufts.edu .
%
% (c) Copyright James M. Murphy, Tufts University, 2020.
%
%
%
%
%
%
%