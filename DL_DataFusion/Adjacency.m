function [A] = Adjacency(X,k,Adjacencyopts)
%Function that computes the weighted adjacency matrix of the graph formed by
%connecting each point of X with its k nearest neighbors (ensures it is
%symmetric; edge is present if either x is among y's knn or vice versa);
%weights are the Euclidean distances

% Adjacencyopts.Method: either 'Simple' or 'FullyConnected';
% if the knn graph is not connected, 'FullyConnected' method will randomly
% sample points from the connected components and add an approximately
% minimal edge connecting the CC's
% Adjacencyopts.NumberofSamplePoints : specify how many random points to
% sample from each connected component

n=size(X,1);
d=size(X,2);

[IDX,D] = knnsearch(X,X,'K',k);
Base=ones(n,k);

for i=1:n
    Base(i,:)=i*Base(i,:);
end

D=D';
IDX=IDX';
Base=Base';

A=sparse(Base(:),IDX(:),D(:));
A = max(A, A');

if strcmp(Adjacencyopts.Method,'FullyConnected')==1
    m = Adjacencyopts.NumberofSamplePoints;
    % Add in edges connected the CC's:
    [~, CC] = AlternateConnComp(A);
    NumberCC = size(unique(CC),2);
    RandomSampleCC = zeros(NumberCC,m);
    for i=1:NumberCC
        CCindex{i} = find(CC==i);
        RandomSampleCC(i,:) = randsample(CCindex{i},m);
    end
    for i=1:NumberCC
        for j=i+1:NumberCC
            SampleDistances = zeros(m,m);
            for s=1:m
                for q=1:m
                    SampleDistances(s,q) = norm(X(RandomSampleCC(i,s),:)-X(RandomSampleCC(j,q),:));
                end
            end
            MinDistance = min(min(SampleDistances));
            [s0, q0] = find(SampleDistances==MinDistance);
            i0 = RandomSampleCC(i,s0(1));
            j0 = RandomSampleCC(j,q0(1));
            A(i0,j0)=MinDistance;
            A(j0,i0)=MinDistance;
        end
    end
            
end
    
end
        