% This directory implements all methods used for comparison.  These are:

% 1.)  Kmeans 
% 2.)  Kmeans with PCA
% 3.)  Kmeans with ICA
% 4.)  Kmeans with random projections
% 5.)  Spectral clustering 
% 6.)  Gaussian mixture models
% 7.)  Sparse manifold clusering and embedding
% 8.)  Heirarchical non-negative matrix factorization
% 9.)  Fast Mumford-Shah 
% 10.) Fast search and find of density peaks clustering
%
% All methods were implemented by J. Murphy, except 3.), 7.), 8.), and 9.).
% These methods were implemented based on toolboxes available online; see
% the corresponding publication of J. Murphy and M. Maggioni for details
% and links.  
%
% References to state-of-the-art comparison methods:
% 
% -Sparse manifold clusering and embedding: E. Elhamifar and R. Vidal. 
% Sparse subspace clustering: Algorithm, theory, and applications. IEEE 
% Transactions on Pattern Analysis and Machine Intelligence, 35(11):2765?2781, 
% 2013.
%
% -Heirarchical non-negative matrix factorization: N. Gillis, D. Kuang, and
% H. Park. Hierarchical clustering of hyperspectral images using rank-two 
% nonnegative matrix factorization. IEEE Transactions on Geoscience and 
% Remote Sensing, 53(4):2066-2078, 2015.
%
% -Fast Mumford-Shah: Z. Meng, E. Merkurjev, A. Koniges, and A. Bertozzi. 
% Hyperspectral video analysis using graph clustering methods. Image 
% Processing On Line, 7:218-245, 2017.
%
% -Fast search and find of density peaks clustering:  A. Rodriguez and A. 
% Laio. Clustering by fast search and find of density peaks. Science, 
% 344(6191):1492-1496, 2014.
%
%
% (c) Copyright James M. Murphy, Tufts University, 2019.
