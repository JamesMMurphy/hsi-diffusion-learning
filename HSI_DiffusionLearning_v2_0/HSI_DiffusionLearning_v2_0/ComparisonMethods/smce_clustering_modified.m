%--------------------------------------------------------------------------
% Copyright @ Ehsan Elhamifar, 2012
%--------------------------------------------------------------------------

function [Y,grp,Yn] = smce_clustering(W,K,dim,gtruth)

if (K == 1)
    gtruth = ones(1,size(W,1));
end

MAXiter = 1000;
REPlic = 20;
N = size(W,1);
K = max(gtruth);

% cluster the data using the normalized symmetric Laplacian 
D = diag( 1./sqrt(sum(W,1)+eps) );
L = eye(N) - D * W * D;
[~,~,V] = svd(L,'econ');
%[~,S,V]=svds(L,K,'smallest');
Yn = V(:,end:-1:end-K+1);
%Yn = V;

for i = 1:N
    Yn(i,:) = Yn(i,1:K) ./ norm(Yn(i,1:K)+eps);
end

if K > 1
    grp = kmeans(Yn(:,1:K),K,'maxiter',MAXiter,'replicates',REPlic,'EmptyAction','singleton');
else 
    grp = ones(1,N);
end
Y = V(:,end-1:-1:end-dim);
Y = (D * Y)';
%Y=0;
% compute the misclassification rate if n > 1
%missrate = missclassGroups(grp,gtruth) ./ length(gtruth); 
