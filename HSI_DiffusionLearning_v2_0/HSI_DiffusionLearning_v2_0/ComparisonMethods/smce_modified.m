%--------------------------------------------------------------------------
% Modification of the original code by Ehsan Elhamifar in 2012
%--------------------------------------------------------------------------

%function [Yc,Yj,clusters,missrate] = smce(Y,lambda,KMax,dim,n,gtruth,verbose)
function EigVecs = smce_modified(X,lambda,KMax,dim,K,gtruth,verbose)

if (nargin < 7)
    verbose = true;
end
if (nargin < 6)
    gtruth = [];
end
if (nargin < 5)
    n = 1;
end
if (nargin < 4)
    dim  = 2;
end

% solve the sparse optimization program
W = smce_optimization(X,lambda,KMax,verbose);
W = processC(W,0.95);

% symmetrize the adjacency matrices
Wsym = max(abs(W),abs(W)');

% get eigenvectors of Laplacian

N=size(X,2);

if N>1e4 % if N is too large, do a work-around to avoid the full SVD
    
    Deg = diag(1./sqrt(sum(W,1)+eps) );
    L=eye(size(X,2))-Deg*W*Deg;
    
    [EigVecs,EigVals]=eigs(L,K,'sm');
    [~,idx]=sort(diag(EigVals),'ascend');
    
    EigVecs=EigVecs(:,idx(1:end));
    
    
    for i=1:N
        EigVecs(i,:)=EigVecs(i,1:K)./norm(EigVecs(i,1:K));
    end
    %}
    
elseif N<= 1e4
    [~,~,EigVecs] = smce_clustering_modified(Wsym,K,dim,gtruth);  
end
