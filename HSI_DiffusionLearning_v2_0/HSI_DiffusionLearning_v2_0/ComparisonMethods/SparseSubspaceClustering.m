function EigVecs = SparseSubspaceClustering(X,Y,K)

% Taken from original SMCE code.  

lambda = 10; 
KMax = 50; 
verbose=0;

EigVecs = smce_modified(X',lambda,KMax,K,K,Y,verbose);

end