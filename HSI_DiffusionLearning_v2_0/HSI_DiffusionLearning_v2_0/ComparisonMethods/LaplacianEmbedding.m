% Compute the normalized eigenvectors of the normalized graph Laplacian.
% Modified from the toolbox of Elhamifar and Vidal.

function [EigVecs, EigVals, W] = SpectralEmbedding(X,K,NN,epsilon)

if strcmp(NN,'all')
    NN=size(X,1);
end

D=MakeAdjacency(X,NN);

if strcmp(epsilon,'adaptive')
    epsilon=.5*sqrt(mean(D(D>0)));
end

D=full(D);
D(D==0)=Inf;
D(logical(eye(size(D))))=0;


W=sparse(exp(-D/epsilon.^2));
Deg=sum(W,1);
L=eye(size(X,1))-diag(Deg.^(-.5))*W*diag(Deg.^(-.5));

try
    [EigVecs,EigVals]=eigs(L+eps,K,'sm');
catch
    [EigVecs,EigVals]=eigs(L+eps*randn(size(L)),K,'sm');
end

for i=1:size(X,1)
    EigVecs(i,:)=EigVecs(i,1:K)./norm(EigVecs(i,1:K));
end

end