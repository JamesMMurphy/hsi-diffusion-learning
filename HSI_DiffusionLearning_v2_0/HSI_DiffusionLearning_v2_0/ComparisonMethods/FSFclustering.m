function Labels = FSFclustering(X,K,DistStruct,Centers)
% Compute a clustering as described in the original fast-search-and-find
% of density peaks paper (Nature 2014).

Labels=zeros(1,size(X,1));

Density=DistStruct.Density;
D=DistStruct.D;

if isempty(Centers)
    
    PWdist=squareform(pdist(X)); %Pairwise distances
    
    [DeltaDensity, Density,~] = FindDensityPeaks(X,K,PWdist,opts);
    
    [~,idx]=sort(DeltaDensity,'descend');
    
    Centers(1)=idx(1);
    
    Needed=K-1;
    start=2;
    % This is a technical script to make sure the cores that are learned are
    % not duplicates.
    
    while Needed>0
        if abs(DeltaDensity(idx(start))-DeltaDensity(Centers(K-Needed)))>1e-6
            Centers(end+1)=idx(start);
            Needed=Needed-1;
        end
        start=start+1;
    end
end
    

for k=1:K
    Labels(Centers(:,k))=k;
end

[~,idx]=sort(Density,'descend');

for j=1:length(Labels)
    if Labels(idx(j))==0
        %NN=knnsearch(X(idx(1:j),:),X(idx(j),:));
        [~,NN]=min(D(idx(j),idx(1:j-1)));
        Labels(idx(j))=Labels(idx(NN));
        if Labels(idx(j))==0 % Special case when many value have same density
            temp=find(Labels>0);
            NN=knnsearch(X(temp,:),X(idx(j),:));
            Labels(idx(j))=Labels(temp(NN));
        end
    end
end


end

