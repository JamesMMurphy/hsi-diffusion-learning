% Implement Gaussian mixture models for HSI clustering

function clusters =ClusterGmm(X,K)

gmfit=fitgmdist(X,K, 'CovarianceType', 'full',  'RegularizationValue', 1e-10);
clusters = cluster(gmfit,X);

end

