function [Centers, G, DistStruct]=DensityPeaksEstimation(X, K, DensityNN,PeakOpts,M,N)

% DeltaDensity is the product of a kernel density estimator and a measure
% of distance from the point to a point of higher kernel density.  Points
% with large values should have both high densities and be far from points
% with higher densities.

PWdist=squareform(pdist(X)); %Pairwise distances

[DeltaDensity, Density, Delta, G, D]=FindDensityPeaks(X,DensityNN,PWdist,PeakOpts,M,N);

DistStruct.DeltaDensity=DeltaDensity;
DistStruct.Density=Density;
DistStruct.Delta=Delta;
DistStruct.D=D;


% Use the largest delta density values to learn cores.

[~,SortedDD_Idx]=sort(DeltaDensity,'descend');

Centers(1)=SortedDD_Idx(1);
Needed=K-1;
start=2;

while Needed>0 && start<=size(X,1)
    %if abs((DeltaDensity(idx(start))-DeltaDensity(Centers(K-Needed)))>1e-7
    Centers(end+1)=SortedDD_Idx(start);
    Needed=Needed-1;
    start=start+1;
end

end
