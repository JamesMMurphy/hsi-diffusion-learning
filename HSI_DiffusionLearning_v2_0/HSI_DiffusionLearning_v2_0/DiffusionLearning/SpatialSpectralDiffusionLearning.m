function [Labels,Labels_Pass1] = SpatialSpectralDiffusionLearning(X,K,DistStruct,Centers,I,J,M,N,WindowSize)

% Compute a clustering as described in the original fast-search-and-find
% of density peaks paper.  Regularize by spatial consensus.

if WindowSize<1
    Labels=FSFclustering(X,K,DistStruct,Centers);
end
    

Labels=zeros(1,size(X,1));

Density=DistStruct.Density;
D=DistStruct.D;

if isempty(Centers)
    
    PWdist=squareform(pdist(X)); %Pairwise distances
    
    [DeltaDensity, Density,~] = FindDensityPeaks(X,K,PWdist,opts);
    
    [~,idx]=sort(DeltaDensity,'descend');
    
    Centers(1)=idx(1);
    
    Needed=K-1;
    start=2;
    % This is a technical script to make sure the cores that are learned are
    % not duplicates.
    
    while Needed>0
        if abs(DeltaDensity(idx(start))-DeltaDensity(Centers(K-Needed)))>1e-6
            Centers(end+1)=idx(start);
            Needed=Needed-1;
        end
        start=start+1;
    end
end

for k=1:K
    Labels(Centers(:,k))=k;
end

[~,idx]=sort(Density,'descend');

%% First pass

for j=1:length(Labels)
    if Labels(idx(j))==0
        [~,NN]=min(D(idx(j),idx(1:j-1)));
        Labels(idx(j))=Labels(idx(NN));
        % Special case when many value have same density
        if Labels(idx(j))==0
            temp=find(Labels>0);
            NN=knnsearch(X(temp,:),X(idx(j),:));
            Labels(idx(j))=Labels(temp(NN));
        end
        % Check spatial consensus
        try
            
            LabelsMatrix=sparse(I(idx(1:j)),J(idx(1:j)),Labels(idx(1:j)),M,N);
            SpatialConsensusLabel=SpatialConsensus(LabelsMatrix,idx(j),M,N,I,J,WindowSize);
            if ~(SpatialConsensusLabel==Labels(idx(j))) && (SpatialConsensusLabel>0)
                Labels(idx(j))=0;
            end
        catch
            keyboard
        end
    end
end

Labels_Pass1=Labels;

%% Second pass

for j=1:length(Labels)
    if Labels(idx(j))==0
        [~,NN]=min(D(idx(j),idx(1:j-1)));
        Labels(idx(j))=Labels(idx(NN));
        % Special case when many value have same density
        if Labels(idx(j))==0
            temp=find(Labels>0);
            NN=knnsearch(X(temp,:),X(idx(j),:));
            Labels(idx(j))=Labels(temp(NN));
        end
        % Enforce spatial consensus if obvious
        try
            LabelsMatrix=sparse(I(idx(1:j)),J(idx(1:j)),Labels(idx(1:j)),M,N);
            [SpatialConsensusLabel,ConsensusFrac]=SpatialConsensus(LabelsMatrix,idx(j),M,N,I,J,WindowSize);
            if ConsensusFrac>.5
                Labels(idx(j))=SpatialConsensusLabel;
            end
        catch
            keyboard
        end
    end
end

end

