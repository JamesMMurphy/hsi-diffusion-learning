function X_Smooth = PatchSmoothing(X,X_Patches,PatchMask,k)
%Run NLM on the data X with patches X_Patches and k the number of nearest
%neighbors;
M=size(X,1);
N=size(X,2);
D=size(X,3);

NN=knnsearch(X_Patches,X_Patches,'K',k);
X_Vec=reshape(X,M*N,D);

for i=1:M
    for j=1:N
        try
            if PatchMask(i,j)
                X_Smooth(i,j,:)=mean(X_Vec(NN(sub2ind([M,N],i,j),:),:),1);
            else
                X_Smooth(i,j,:)=X(i,j,:);
            end
        catch
            keyboard
        end
    end
end

X_Smooth=reshape(X_Smooth,M*N,size(X_Smooth,3));
X_Smooth=X_Smooth./repmat(sqrt(sum(X_Smooth.*X_Smooth,1)),size(X_Smooth,1),1); %Normalize X


end

