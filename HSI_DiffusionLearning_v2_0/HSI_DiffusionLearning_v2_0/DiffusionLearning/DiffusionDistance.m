function  [D,G]= DiffusionDistance(X,t,DiffusionOpts)

% Compute the diffusion distance using the first k eigenvectors at time t.

if isfield(DiffusionOpts, 'NN_Idx')
    Opts.DistInfo.idxs=DiffusionOpts.NN_Idx;
    Opts.DistInfo.counts=DiffusionOpts.NN_Counts;
    Opts.DistInfo.dists=DiffusionOpts.NN_Dists;
end

if strcmp(DiffusionOpts.K,'automatic')
    Opts.kEigenVecs=10;
    Opts.kNN=DiffusionOpts.kNN;
    Opts.Epsilon=1;
    Opts.Normalization='markov';
    G = GraphDiffusion(X',0,Opts);
    [~,Opts.kEigenVecs]=max(-diff(G.EigenVals(1:8)));
    G.EigenVals=G.EigenVals(1:Opts.kEigenVecs);
    G.EigenVecs=G.EigenVecs(:,1:Opts.kEigenVecs);    
else
    Opts.kEigenVecs=DiffusionOpts.K;
    Opts.kNN=DiffusionOpts.kNN;
    Opts.Epsilon=DiffusionOpts.epsilon;
    Opts.Normalization='markov';
    G = GraphDiffusion(X',0,Opts);
end

WeightedEigVecs=(G.EigenVals.^t)'.*G.EigenVecs;

D=squareform(pdist(real(WeightedEigVecs)));

end

