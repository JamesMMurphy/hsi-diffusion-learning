% This directory contains all the functions needed to do diffusion learning
% (DL), spatial-spectral diffusion learning (DLSS), and spatially-regularized
% diffusion learning (SRDL).
%
% (c) Copyright James M. Murphy, Tufts Univetsity, 2019.