function [DeltaDensity, Density, Delta, G, PWdist] = FindDensityPeaks(X,K,D,PeakOpts,M,N)

%Compute the density peaks of the dataset X.
% Compute the true finite-sample Density and the distances Delta from each point to the
% point of higher density.

%% Compute density and initialize Delta

if nargin<3
    [Density,PWdist]=LocalDensity(X,K);
else 
   [Density,PWdist]=LocalDensity(X,K,D);
end

Delta=zeros(size(Density));

%% Compute pairwise distances to be used for Delta computation

if strcmp(PeakOpts.ModeDetection, 'Euclidean')
    PWdist=squareform(pdist(X));
    G=[];
    
elseif  strcmp(PeakOpts.ModeDetection, 'Diffusion') || strcmp(PeakOpts.ModeDetection, 'PatchDiffusion')
    [PWdist,G]=DiffusionDistance(X,PeakOpts.DiffusionTime,PeakOpts.DiffusionOpts);
    
elseif strcmp(PeakOpts.ModeDetection, 'SpatialDiffusion') || strcmp(PeakOpts.ModeDetection, 'SpatialPatchDiffusion')
    
    X_spatial=reshape(X,M,N,size(X,2));
    
    NN_Idx=cell(M,N);
    NN_Counts=zeros(M,N);
    NN_Dists=cell(M,N);
    
    for i=1:M
        for j=1:N
            NN_Idx{i,j}=FindNeighbors([i,j], PeakOpts.Window_Diffusion, M, N);
            NN_Counts(i,j)=length(NN_Idx{i,j});
            DistTemp=...
            sqrt(sum((repmat(squeeze(X_spatial(i,j,:))',size(NN_Idx{i,j},2),1)-X(sub2ind([M,N], NN_Idx{i,j}(1,1:size(NN_Idx{i,j},2)), NN_Idx{i,j}(2,1:size(NN_Idx{i,j},2))),:)).^2,2))';
            [~,Idx]=sort(DistTemp,'ascend');
            NN_Idx{i,j}=NN_Idx{i,j}(:,Idx);
            NN_Idx{i,j}=sub2ind([M,N],NN_Idx{i,j}(1,:),NN_Idx{i,j}(2,:));% Convert back to subscript
            NN_Dists{i,j}=DistTemp(Idx);
        end
    end
        
    PeakOpts.DiffusionOpts.NN_Idx=reshape(NN_Idx,M*N,1);
    PeakOpts.DiffusionOpts.NN_Counts=reshape(NN_Counts,M*N,1);
    PeakOpts.DiffusionOpts.NN_Dists=reshape(NN_Dists,M*N,1);
    PeakOpts.DiffusionOpts.epsilon=mean(cat(2,NN_Dists{:}));

    [PWdist,G]=DiffusionDistance(X,PeakOpts.DiffusionTime,PeakOpts.DiffusionOpts);
    
    
end

%% Compute Delta by measuring the minimal distance to a point of higher density

for i=1:length(Density)
    if ~(Density(i)==max(Density))
        Delta(i)=min(PWdist(Density>Density(i),i));     
    else
        Delta(i)=max(PWdist(i,:)); 
    end
end

%% Normalize 

Delta=Delta/max(Delta);
Density=Density/sum(Density);

%% Combine these measures multiplicatively, as suggested in FSFDPC paper (Science, 2014)

DeltaDensity=Delta.*(Density);

end