function [X_Patches, Patches, PatchMask] = ComputePatches(X,p)

% Computes patches of size pxp in the data X.


M=size(X,1);
N=size(X,2);
D=size(X,3);

PatchMask=zeros(M,N);
Patches=zeros(M,N,D*(1+2*p)^2);


for i=1:M
    for j=1:N
        if (i-p>=1) && (i+p<=M) && (j-p>=1) && (j+p<=N)
            try
                PatchMask(i,j)=1;
                Temp=X(i-p : i+p,j-p : j+p, :);
                Temp=Temp(:);
                Patches(i,j,:)=Temp;
            catch
                keyboard
            end
        end
    end
end

X_Patches=reshape(Patches,size(Patches,1)*size(Patches,2),size(Patches,3));
X_Patches=X_Patches./repmat(sqrt(sum(X_Patches.*X_Patches,1)),size(X_Patches,1),1); %Normalize X_Patches