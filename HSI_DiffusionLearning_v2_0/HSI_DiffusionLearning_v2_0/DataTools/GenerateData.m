function [X,Y,Ytuple,K_GT,d,DataParameters,Window_Diffusion,HSI_Cube,HSI_BorderCube]=GenerateData(DataInput)
 
%%  Use real HSI data
if strcmp(DataInput.datasource,'HSI')
    
    [X,Y,Ytuple,d,K_GT,DataParameters,Window_Diffusion]=MakeHSI(DataInput.HSIname,DataInput.PatchSize);
    
end