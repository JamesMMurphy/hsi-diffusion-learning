% All real data and scripts for generating synthetic data appear in this
% directory.  The APL data used for anomaly detection cannot be made
% public; the remaining HSI images are standard, and can be found online at 
% http://www.ehu.eus/ccwintco/index.php?title=Hyperspectral_Remote_Sensing_Scenes
% 
% (c) Copyright James M. Murphy, Tufts University, 2019.