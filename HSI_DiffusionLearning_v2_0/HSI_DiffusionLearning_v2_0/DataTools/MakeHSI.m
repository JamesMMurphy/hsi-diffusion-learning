% Load HSI and necessary related variables

function [X,Y,Ytuple,d,K_GT,DataParameters,Window_Diffusion] = MakeHSI(HSIname,PatchSize)

BorderLength=(PatchSize-1)/2;

if strcmp(HSIname,'IP')
    load('indian_pines.mat');
    load('indian_pines_gt.mat');
    X=indian_pines(1:50,1:25,:);
    Y=indian_pines_gt(1:50,1:25);
    RawHSI_Border=indian_pines(1:50,1:25,:);
    Y=Y';
    X=permute(X,[2,1,3]);
    RawHSI_Border=permute(RawHSI_Border,[2,1,3]);
    Window_Diffusion=8;
end

if strcmp(HSIname,'IP_Full')
    load('indian_pines.mat');
    load('indian_pines_gt.mat');
    X=indian_pines;
    Y=indian_pines_gt;
    RawHSI_Border=indian_pines;
    Y=Y';
    X=permute(X,[2,1,3]);
    RawHSI_Border=permute(RawHSI_Border,[2,1,3]);
    Window_Diffusion=8;
end

if strcmp(HSIname,'Pavia')
    
    load('Pavia.mat');
    load('Pavia_gt.mat');

    X=pavia(101+BorderLength:400-BorderLength,241+BorderLength:300-BorderLength,:);
    RawHSI_Border=pavia(101:400,241:300,:);
    X=permute(X,[2,1,3]);
    RawHSI_Border=permute(RawHSI_Border,[2,1,3]);
    Y=double(pavia_gt(101+BorderLength:400-BorderLength,241+BorderLength:300-BorderLength));
    Y=Y';
    Window_Diffusion=20;
end

if strcmp(HSIname,'SalinasA')
    
    load('SalinasA_smallNoise.mat');
    load('SalinasA_gt.mat');
    
    RawHSI_Border=X;
    X=X(1+BorderLength:86-BorderLength,1+BorderLength:83-BorderLength,:);
    
    Y=double(salinasA_gt);
    Y=Y(1+BorderLength:end-BorderLength,1+BorderLength:end-BorderLength);
    X=permute(X,[2,1,3]);
    RawHSI_Border=permute(RawHSI_Border,[2,1,3]);   
    Window_Diffusion=20;
end

if strcmp(HSIname,'KSC')
    
    load('KSC.mat');
    load('KSC_gt.mat');
    
    X=KSC(221:320,301:550,:);
    RawHSI_Border=X;
    X=X(1+BorderLength:100-BorderLength,1+BorderLength:250-BorderLength,:);
    Y=double(KSC_gt(221:320,301:550));
    Window_Diffusion=20;
    
end

%% Only work with ground truth if there is some.

if ~isempty(Y)
    DataParameters.RawHSI=X;
    DataParameters.RawHSI_Border=RawHSI_Border;
    [DataParameters.M, DataParameters.N,~]=size(DataParameters.RawHSI);
    
    GT=Y;
    Y=reshape(Y,size(Y,1)*size(Y,2),size(Y,3));
    Y=Y';
    
    Y=Y(Y>0);
    Y=UniqueGT(Y);
    
    K_GT=max(Y);
    
    Ytuple=zeros(max(Y),size(Y,2));
    
    for k=1:size(Y,2)
        Ytuple(Y(k),k)=1;
    end
    
    DataParameters.GT=GT;
    
    %%
    
    X=DataParameters.RawHSI;
    X=reshape(X,size(X,1)*size(X,2),size(X,3));
    X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1); %Normalize X
    
    %%
    Y=reshape(DataParameters.GT,1,size(GT,1)*size(GT,2));
    
    d=size(X,2);
    
end

end

