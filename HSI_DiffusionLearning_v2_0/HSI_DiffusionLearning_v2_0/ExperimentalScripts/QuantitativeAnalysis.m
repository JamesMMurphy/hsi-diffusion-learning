%% Compute OA, AA, kappa for all methods.

%Performance quantitative analysis only on labeled pixels.
Yuse=UniqueGT(Y(Y>0));

if ~isempty(Y)
    
    %Raw kmeans
    [OA_Kmeans,AA_Kmeans,Kappa_Kmeans]=GetAccuracies(Labels_Kmeans(Y>0),Yuse,K_Learned);
    
    %Kmeans on PC projections
    [OA_PCA,AA_PCA,Kappa_PCA]=GetAccuracies(Labels_PCA(Y>0),Yuse,K_Learned);
    
    %Kmeans after random projections
    [OA_RandProj,AA_RandProj,Kappa_RandProj]=GetAccuracies(Labels_RandProj(Y>0),Yuse,K_Learned);
    
    %kmeans on independent componenents
    [OA_ICA,AA_ICA,Kappa_ICA]=GetAccuracies(Labels_ICA(Y>0),Yuse,K_Learned);
    
    %Spectral clustering
    [OA_SC,AA_SC,Kappa_SC]=GetAccuracies(Labels_SC(Y>0),Yuse,K_Learned);
    
    %GMM
    [OA_GMM,AA_GMM,Kappa_GMM]=GetAccuracies(Labels_GMM(Y>0),Yuse,K_Learned);
 
    %SMCE
    [OA_SMCE,AA_SMCE,KappaSMCE]=GetAccuracies(Labels_SMCE(Y>0),Yuse,K_Learned);
    
    %Hierarchical NMF
    [OA_NMF,AA_NMF,KappaNMF]=GetAccuracies(Labels_NMF(Y>0)',Yuse,K_Learned);
    
    %FSFDPC
    [OA_FSFDPC,AA_FSFDPC,Kappa_FSFDPC]=GetAccuracies(Labels_FSFDPC(Y>0),Yuse,K_Learned);
    
    %Diffusion Learning
    [OA_DL,AA_DL,Kappa_DL]=GetAccuracies(Labels_DL(Y>0),Yuse,K_Learned);
    
    %Diffusion learning spatial-spectral
    [OA_DLSS,AA_DLSS,Kappa_DLSS]=GetAccuracies(Labels_DLSS(Y>0),Yuse,K_Learned);

    %Diffusion learning spatial-spectral
    [OA_SRDL,AA_SRDL,Kappa_SRDL]=GetAccuracies(Labels_SRDL(Y>0),Yuse,K_Learned);

    %Diffusion learning spatial-spectral
    [OA_DLSSP,AA_DLSSP,Kappa_DLSSP]=GetAccuracies(Labels_DLSSP(Y>0),Yuse,K_Learned);
end