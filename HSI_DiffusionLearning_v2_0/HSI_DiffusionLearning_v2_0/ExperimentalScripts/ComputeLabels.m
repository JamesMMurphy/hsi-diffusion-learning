% Compute labels for each method.

%% Some methods (active learning) require GT.  If it does not exist, make a zero matrix for it.

if isempty(GT)
    [I,J]=find(sum(DataParameters.RawHSI,3)~=0);
else
   [I,J]=find(GT>-1);
end

%% Kmeans on raw data

tic;
[Labels_Kmeans,~]=kmeans(X,K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations);
Labels_Kmeans=Labels_Kmeans';
Time_Kmeans=toc;

%% Kmeans + PCA

tic;
Labels_PCA=kmeans(X*V(:,1:K_Learned),K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations)';
Time_PCA=toc+Time_PCA;

%% Kmeans + ICA

tic
Labels_ICA=kmeans(ICA',K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations)';
Time_ICA=toc+Time_ICA;

%% Kmeans + random projection

tic;
Labels_RandProj=kmeans(X*RP,K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations)';
Time_RandProj=toc+Time_RandProj;

%% Gaussian mixture model

tic;
Labels_GMM=ClusterGmm(X,K_GT)';
Time_GMM=toc;

%% Spectral clustering

tic;
Labels_SC=kmeans(EigVecsLE,K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations)';
Time_SC=toc+Time_SC;

%% SMCE

tic;
Labels_SMCE=kmeans(EigVecsSMCE,K_Learned,'Replicates', NumReplicates, 'MaxIter', MaxIterations)';
Time_SMCE=toc+Time_SMCE;

%% Hierarchical NMF

tic;
Labels_NMF=hierclust2nmf((X-min(min(X)))',K_Learned);
Time_NMF=toc;

%% FSFDPC

tic;
Labels_FSFDPC=FSFclustering(X,K_GT,DistStructEuclidean,CentersEuclidean);
Time_FSFDPC=toc+Time_FSFDPC_1;

%% Diffusion Learning (DL)

tic;
Labels_DL=FSFclustering(X,K_GT,DistStructDiffusion,CentersDiffusion);
Time_DL=toc+Time_DL_1;

%% Spatial-Spectral Diffusion Learning (DLSS)

tic;
[Labels_DLSS,Labels_DLSS_Pass1]=SpatialSpectralDiffusionLearning(X,K_GT,DistStructDiffusion,CentersDiffusion,I,J,M,N,SpatialWindowSize);
Time_DLSS=toc+Time_DL_1;

%% Spatial-Spectral Diffusion Learning With Regularized Diffusion Distances (SRDL)

tic;
[Labels_SRDL,Labels_SRDL_Pass1]=SpatialSpectralDiffusionLearning(X,K_GT,DistStructSpatialDiffusion,CentersSpatialDiffusion,I,J,M,N,SpatialWindowSize);
Time_SRDL=toc+Time_SRDL_1;

%% Patch-based Diffusion Learning (DLSSP)

tic;
[Labels_DLSSP,Labels_DLSSP_Pass1]=SpatialSpectralDiffusionLearning(X_Patches,K_GT,DistStructPatchDiffusion,CentersPatchDiffusion,I,J,M,N,SpatialWindowSize);
toc;
