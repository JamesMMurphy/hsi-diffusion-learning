% Test DL and DLSS against a variety of comparison methods.

profile off;
profile on;

clear all;

%% Ask user to choose dataset

prompt = 'Which dataset? \n 1.) Indian Pines Subset \n 2.) Pavia Subset\n 3.) Salinas A Full Dataset \n';
DataSetSelected = input(prompt);

if DataSetSelected==1
    DataInput.datasource='HSI';
    DataInput.HSIname='IP';
    DataInput.idx1=1:50;
    DataInput.idx2=1:25;
    PeakOpts.Window_Diffusion=8;
    DataInput.PatchSize=3;

elseif DataSetSelected==2
    DataInput.datasource='HSI';
    DataInput.HSIname='Pavia';
    DataInput.idx1=101:400;
    DataInput.idx2=241:300;
    PeakOpts.Window_Diffusion=20;
    DataInput.PatchSize=3;
    
elseif DataSetSelected==3
    DataInput.datasource='HSI';
    DataInput.HSIname='SalinasA';
    DataInput.idx1=1:83;
    DataInput.idx2=1:86;
    PeakOpts.Window_Diffusion=20;
    DataInput.PatchSize=3;


end

[X,Y,Ytuple,K_GT,d,DataParameters]=GenerateData(DataInput);
GT=DataParameters.GT;
M=DataParameters.M;
N=DataParameters.N;

%% Set Parameters

%Data name
PeakOpts.Data=DataInput.HSIname;

%Should we plot everything or not?  Should we save things?
UserPlot=1;
SavePlots=0;

%Detect number of eigenvectors to use automatically
PeakOpts.DiffusionOpts.K='automatic'; 

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100;

%Set epsilon for weight matrix; use same one for spectral clustering
PeakOpts.DiffusionOpts.epsilon=1;

% Set time to evaluate diffusion distance
PeakOpts.DiffusionTime=30;

%Kmeans parameters
NumReplicates=10;
MaxIterations=100;

%Set how many cores we want to learn.  If K_Learned is different from
%K_GT, it will not be possible to compare easily and immediately against
%the ground truth, though other patterns may be studied in this way.
K_Learned=K_GT;

%How many nearest neighbors to use for KDE
DensityNN=20;

%Spatial window size for labeling regime
SpatialWindowSize=3;

%% Learn modes with Euclidean and diffusion distances

% Euclidean distances (FSFDPC)
PeakOpts.ModeDetection='Euclidean';

tic;
[CentersEuclidean, G_euc, DistStructEuclidean] = DensityPeaksEstimation(X, K_Learned, DensityNN, PeakOpts, M, N);
Time_FSFDPC_1=toc;

% Diffusion distances  (DL, DLSS)
PeakOpts.ModeDetection='Diffusion';

tic;
[CentersDiffusion, G_dif, DistStructDiffusion] = DensityPeaksEstimation(X, K_Learned, DensityNN, PeakOpts, M, N);
Time_DL_1=toc;

% Diffusion distances with spatial regularization (SRDL)
PeakOpts.ModeDetection='SpatialDiffusion';

tic;
[CentersSpatialDiffusion, G_Spatialdif, DistStructSpatialDiffusion] = DensityPeaksEstimation(X, K_Learned, DensityNN, PeakOpts, M, N);
Time_SRDL_1=toc;

% Diffusion distances on patch space (DLSSP)
PeakOpts.ModeDetection='PatchDiffusion';

tic;
[X_Patches, Patches, PatchMask]=ComputePatches(DataParameters.RawHSI,DataInput.PatchSize);
X_Smooth = PatchSmoothing(DataParameters.RawHSI,X_Patches,PatchMask,10);
[CentersPatchDiffusion, G_PatchDiff, DistStructPatchDiffusion] = DensityPeaksEstimation(X_Smooth, K_Learned, DensityNN, PeakOpts, M, N);
Time_SRDL_1=toc;

%% Compute features for comparison methods

%Principal components
tic;
V=GetPC(X);
Time_PCA=toc;

%Independent components
tic;
ICA=fastica(X', 'firsteig',1,'lasteig',K_Learned, 'verbose','off','stabilization','on');
Time_ICA=toc;

%Gaussian random projection
tic;
RP= randn(size(X,2),K_Learned)/sqrt(K_Learned);
RP=RP./sqrt(sum(RP.^2));
Time_RandProj=toc;

%Eigenvectors of normalized Laplacian
tic;
[EigVecsLE, EigValsLE]=LaplacianEmbedding(X,K_Learned,PeakOpts.DiffusionOpts.kNN,PeakOpts.DiffusionOpts.epsilon);
Time_SC=toc;

%SMCE
tic;
EigVecsSMCE=SparseSubspaceClustering(X,Y,K_GT);
Time_SMCE=toc;

%% Compute all labels

ComputeLabels;

%% Compute OA, AA, Kappa after aligning with the Hungarian algorithm

QuantitativeAnalysis;

%% Make images of the clusterings

if UserPlot
    MakePlots
end