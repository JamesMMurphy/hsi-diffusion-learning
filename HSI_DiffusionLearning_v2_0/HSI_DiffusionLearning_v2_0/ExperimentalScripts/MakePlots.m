%% Make Plots for all data

close all;

%% HSI examples

if strcmp(DataInput.datasource,'HSI')
    
    if isempty(GT)
        GT=reshape(Labels_Kmeans,M,N);
    end
    
    close all;
    
    [I,J]=find(GT>0);
    
    GT=UniqueGT(GT(GT>0));
    
    GT=sparse(I,J,GT,M,N);
    
    %%  GT

    h=figure;
    imagesc(GT);
    axis equal tight;
    axis off;
  
    if SavePlots
        saveas(h,[DataInput.HSIname,'_GT.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_GT.pdf']);
        savefig([DataInput.HSIname,'_GT']);
    end
       
   
    %% Kmeans
    
    h=figure;
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_Kmeans(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_Kmeans.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_Kmeans.pdf']);
        savefig([DataInput.HSIname,'_Kmeans']);
    end
    
    %% PCA
   
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_PCA(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;    
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_PCA.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_PCA.pdf']);
        savefig([DataInput.HSIname,'_PCA']);
    end
    
    %% ICA
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_ICA(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;     
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_ICA.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_ICA.pdf']); 
        savefig([DataInput.HSIname,'_ICA']);
    end
    
     %% Random Projection
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_RandProj(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;     
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_RP.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_RP.pdf']);
        savefig([DataInput.HSIname,'_RandomProj']);
    end
    
    %% Spectral Clustering
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_SC(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_SC.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_SC.pdf']);
        savefig([DataInput.HSIname,'_SC']);
    end
    
    %% GMM
    
    h=figure;
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_GMM(GT>0),K_Learned),M,N));
    axis off;
    axis equal tight;    
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_GMM.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_GMM.pdf']);
        savefig([DataInput.HSIname,'_GMM']);
    end
    
    %% SMCE
    
    h=figure;
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_SMCE(GT>0),K_Learned),M,N));
    axis off;
    axis equal tight;    
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_SMCE.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_SMCE.pdf']);
        savefig([DataInput.HSIname,'_SMCE']);
    end
    
    %% NMF
    
    h=figure;
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_NMF(GT>0)',K_Learned),M,N));
    axis off;
    axis equal tight;    
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_NMF.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_NMF.pdf']);
        savefig([DataInput.HSIname,'_NMF']);
    end
    
    %% FSFDPC
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_FSFDPC(GT>0),K_Learned),M,N));
    axis off;
    axis equal tight;    
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_FSFDPC.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_FSFDPC.pdf']);
        savefig([DataInput.HSIname,'_FSFDPC']);
    end
    
    %% Diffusion Learning
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_DL(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;     
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_DL.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_DL.pdf'])
        savefig([DataInput.HSIname,'_DL']);
    end
    
    %% Diffusion learning spatial-spectral
    
    h=figure; 
    imagesc(sparse(I,J,AlignClustersHungarian(GT(GT>0)',Labels_DLSS(GT>0),K_Learned),M,N));
    axis off; 
    axis equal tight;     
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_DLSS.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_DLSS.pdf'])
        savefig([DataInput.HSIname,'_DLSS']);
    end
    
    %% Sum of all HSI bands
    
    h=figure; 
    imagesc(sum(DataParameters.RawHSI,3));
    axis off;
    axis equal tight;
    
    if SavePlots==1
        saveas(h,[DataInput.HSIname,'_BandSum.pdf']);
        system(['/usr/local/bin/pdfcrop --verbose ',DataInput.HSIname,'_BandSum.pdf']);
        savefig([DataInput.HSIname,'_BandSum']);
    end

    
end