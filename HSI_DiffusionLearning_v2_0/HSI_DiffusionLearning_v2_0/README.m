% This toolbox implements several hyperspectral clustering algorithms 
% develped by James M. Murphy (Tufts University) and Mauro Maggioni 
% (Johns Hopkins University). The algorithms iimplemented are:
%
% 1.)  Diffusion Learning (DL)
% 2.)  Spectral-Spatial Diffusion Learning (DLSS)
% 3.)  Spatially-Regularized Diffusion Learning (SRDL)
% 4.)  Spectral-Spatial Patch-Based Diffusion Learning (DLSSP)
%
% Those who wish to use and edit this code are free to do so.  We request 
% that if this code is used in any way for a publication, the following 
% papers be cited:
%
%
% -Murphy, James M., and Mauro Maggioni. 
% "Unsupervised Clustering and Active Learning of Hyperspectral Images With Nonlinear Diffusion." 
% IEEE Transactions on Geoscience and Remote Sensing 57.3 (2019): 1829-1845.
%
% -Murphy, James M., and Mauro Maggioni. 
% "Spectral-Spatial Diffusion Geometry for Hyperspectral Image Clustering." 
% IEEE Geoscience and Remote Sensing Letters 17.7 (2020): 1243-1247.
%
% In order to run the main learning algorithms, one auxilliary directory is 
% required:
%
% DiffusionGeometry: http://www.math.jhu.edu/~mauro/#tab_DiffusionGeom
%
% The main demonstration script is DiffusionLearningDemo.  To perform the 
% comparisons called in this script, the following auxilliary directories 
% are required:
%
% Sparse Manifold Clustering and Embedding (SMCE): http://www.vision.jhu.edu/code/
% Heirarchical Non-Negative Matrix Factorization (HNMF): https://sites.google.com/site/nicolasgillis/code
% Fast Independent Component Analysis (ICA): https://www.cs.helsinki.fi/u/ahyvarin/papers/fastica.shtml
%
% In order to perform cluster alignment with the Hungarian algorithm, the
% following package is required: https://www.mathworks.com/matlabcentral/fileexchange/20328-munkres-assignment-algorithm
%
% 
%
% (c) Copyright James M. Murphy, Tufts University, 2020.
