% All real data for anomaly detection cannot be made
% public; the remaining HSI images are standard, and can be found online at 
% http://www.ehu.eus/ccwintco/index.php?title=Hyperspectral_Remote_Sensing_Scenes
% 
% (c) Copyright Johns Hopkins University, 2017, James M. Murphy
